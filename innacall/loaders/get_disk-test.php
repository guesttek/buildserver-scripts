#!/usr/bin/php
<?php
#   get_disk.php ${lodtype} ${skind} ${versuffix} ${ddisk}
$vsuffix = trim($argv[3]);
$btyp = trim($argv[1]);
$ddisk = trim($argv[4]);
$skind = trim($argv[2]);

/*
exec("hdparm -i /dev/$ddisk",$hdparm);
$hdparm3=explode(",",$hdparm[3]);
# $hdparm5=explode(",",$hdparm[5]);
$hdmod1=explode("=",$hdparm3[0]);
$hdmodel=trim($hdmod1[1]);
$hdser1=explode("=",$hdparm3[2]);
$hdserial=trim($hdser1[1]);
*/

##################################
# read HDPARM for hard drive
# parameters.. serial..brand..
# CHS.. put together in string and
# take sha1 for key 14
##################################
exec("hdparm -i /dev/$ddisk 2>&1",$hdparm);
#
# print_r($hdparm);
# $hdparm3=explode(",",$hdparm[3]);
# $hdparm5=explode(",",$hdparm[5]);

# does our parm 3 for model and serial exist
if (array_key_exists(3,$hdparm) && strpos($hdparm[3],"Model") !== FALSE && strpos($hdparm[3],"SerialNo") !== FALSE) {
  $hdparm3=explode(",",$hdparm[3]);
  $hdmod1=explode("=",$hdparm3[0]);
  $hdmodel=trim($hdmod1[1]);
  $hdser1=explode("=",$hdparm3[2]);
  $hdserial=trim($hdser1[1]);
} else {
  # default our HDD stuff
  $hdmodel = "Unknown";
  $hdserial="Unknown";
}

exec("sfdisk -g /dev/$ddisk",$sfdisk);
# break off from the disk name
$hdchs1=explode(":",$sfdisk[0]);
# break apart at the comma
$hdchs=trim($hdchs1[1]);
$hdchs_break=explode(",",$hdchs);
# trim our results
$cyls_raw = trim($hdchs_break[0]);
$heads_raw = trim($hdchs_break[1]);
$sectors_raw = trim($hdchs_break[2]);
# bust at the spaces for real results
# $hddd = explode("/",$hdchs);
$cyls_ready = explode(" ",$cyls_raw);
$heads_ready = explode(" ",$heads_raw);
$sectors_ready = explode(" ",$sectors_raw);

$cyls = trim($cyls_ready[0]);
$heads = trim($heads_ready[0]);
$sectors = trim($sectors_ready[0]);

echo "Storage model:$hdmodel:\nStorage Serial:$hdserial:\nStorage Geometry: Cylinders:$cyls Heads:$heads Sectors:$sectors:\n";
## make a file
echo "DISK FILE: /mnt/nfs/innacall/system_install_".$btyp."_".$skind."_".$vsuffix."/diskers.txt\n";
$rrr = fopen("/mnt/nfs/innacall/system_install_".$btyp."_".$skind."_".$vsuffix."/diskers.txt",'w+');
if ($rrr) {
  fwrite($rrr,$cyls."\n");
  fwrite($rrr,$heads."\n");
  fwrite($rrr,$sectors."\n");
}
fclose($rrr);
sleep(4);

?>
