
## does flash card exist?
if [ ! -e /dev/$ddisk ]
then
  info "FATAL!! No First Storage Device Found! Power Off and Check Storage Devices for proper connection"
  exit
else
	info "Storage Disk Found. Proceeding to Partition it"

	versuffix=
	lodtype=
	verlod=0
	while [ $verlod -ne 1 ];
	do
		cony=
  		while [ "$cony" != "y" ];
       		do
               		echo "Please enter the Software Load name"
			echo "a - Alpha Load"
			echo "b - beta"
			echo "c - Release Candidate"
			echo "r - Release Version"
			echo "u - Custom Name"
			echo -n "(a/b/c/r/u):"
                	## ask to deploy php files or not? this may be an update to other files.
               		read conylod
			case $conylod in
				a)
					lodtype=alpha
					;;
				b)
					lodtype=beta
					;;
				c)
					lodtype=rc
					;;
				r)
					lodtype=release
					;;
				u)
					echo -n "Enter Custom Load Name:"
					read lodtype
					;;
				*)
					echo "ERROR! Must type a/b/c/r/u!!"
#					cony=n
					break
					;;
			esac
			echo -n "Please enter the version suffix you are installing:"
			read versuffix
			echo "Installing system_install_${lodtype}_${skind}_${versuffix}"
			echo -n "Is this correct? (y/n):"
			read cony
               		if [ "$cony" = "y" ];
               		then
               	        	# see if our dir exists
				if [ -e /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix} ];
				then
					verlod=1
				else
					echo system_install_${lodtype}_${skind}_${versuffix} Not Found!!!
					echo Please Try Again!
					verload=0
					cony=n
					lodtype=
					versuffix=
#                       		echo -n "Please enter an IP or Hostname for NFS Storage Location"
#                       		read store
               			fi
			fi
        	done
	done
fi
#echo -n "Please enter the version suffix you are installing:"
#read versuffix
echo "Test ended"

## does flash card parameters files exist?
if [ ! -e /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix}/hdd_part.txt ]
then
  info "FATAL!! No Disk Parameters file Found! Contact System Administrator!"
  exit
fi

if [ -e /dev/${ddisk}1 ]
then
  info "Disk Appears to have Data!!! Please Confirm Overwrite!"
  echo -n "Overwrite ${skind} Disk with ${lodtype} image? y/n then press [ENTER]:"
  read conny
  if [ "$conny" != "y" ]
  then
    info "Partitioning Cancelled. Assuming already partitioned correctly!"
    partition=false
  else
    partition=true
  fi
fi

if $partition
then
  ## lets partition our flash

  info "getting Actual Disk Geometry. Please wait...."
  /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix}/get_disk.php ${versuffix} ${lodtype} ${skind}

  if [ ! -e /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix}/diskers.txt ]
  then
    sleep 1
    info "Unable to get disk geometry!!!! Aborting Install."
    exit
  fi

## get parameters from diskers file
  old_IFS=$IFS
  IFS=$'\n'
  lines=( $(cat /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix}/diskers.txt) )
  IFS=$old_IFS
  sleep 1
  info "Partitioning Storage Drive. Please wait...."
  sleep 1
  sfdisk --force -C${lines[0]} -H${lines[1]} -S${lines[2]} /dev/$ddisk < /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix}/hdd_part.txt
  sleep 10
  rm -f /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix}/diskers.txt

  ####### reload udev to bring new hds devices online #########
  udevd --daemon

  if [ $UDEVVERSION -lt 140 ]; then
    UDEV_LOG_PRIO_ARG=--log_priority
    UDEV_QUEUE_EMPTY="udevadm settle --timeout=1"
  fi

  udevadm trigger $udevtriggeropts  >/dev/null 2>&1

  i=0
  while [ $i -le 5 ]
    do
    check_finished && break

    udevsettle

    check_finished && break

    if [ -f /initqueue/work ]; then
        rm /initqueue/work
    fi
    for job in /initqueue/*.sh; do
        [ -e "$job" ] || break
        job=$job . $job
        check_finished && break 2
    done

    $UDEV_QUEUE_EMPTY >/dev/null 2>&1 || continue

    modprobe scsi_wait_scan && rmmod scsi_wait_scan

    $UDEV_QUEUE_EMPTY >/dev/null 2>&1 || continue

    for job in /initqueue-settled/*.sh; do
        [ -e "$job" ] || break
        job=$job . $job
        check_finished && break 2
    done

    $UDEV_QUEUE_EMPTY >/dev/null 2>&1 || continue

    # no more udev jobs and queues empty.
    sleep 0.5
    i=$(($i+1))
  done
  unset job
  unset queuetriggered
  # set licenseonly to false since we partitioned the drive
  licenseonly=false
else
  echo -n "Create Licenses only? y/n then press [ENTER]:"
  read liconly
  if [ "$liconly" = "y" ]
  then
    info "License Only mode. not writing filesystems."
    licenseonly=true
  else
    licenseonly=false
  fi
fi

if ! $licenseonly
then
  ## disk is partitioned, lets make a file system on the boot partition.

  info "Creating Boot Filesystem..."
  mkfs.ext3 /dev/${ddisk}1

  # disable filesystem checking for this device. it can cause systems to hang on boot

  info "Disabling Automatic Boot Filesystem Checking."
  tune2fs -i 999 -c 999 /dev/${ddisk}1

  # boot filesystem created. lets make the user partition
  info "Creating User Filesystem..."
  mkfs.ext3 /dev/${ddisk}4

  # disable filesystem checking for this device. it can cause systems to hang on boot

  info "Disabling Automatic User Filesystem Checking."
  tune2fs -i 999 -c 999 /dev/${ddisk}4

  sleep 10
  # bring over image of boot partition
  ddrescue --force /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix}/${ddisk}1.img /dev/${ddisk}1

  # install grub bootloader to the boot sector of the HDD

  cd /
  #/bin/bash grubber.sh
  grub <<EOF
root (hd0,0)
setup (hd0)
quit
EOF

  sleep 10
  info "Loading Default System database files....."

  ## does default database tar exist?
  if [ ! -e /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix}/${ddisk}4.tar ]
  then
    info "FATAL!! Default system Database file NOT Found! Contact System Administrator!"
    exit
  fi

  ## DB file exists..  lets extract it out
  info "Mounting User Partition....."
  sleep .5
  mount /dev/${ddisk}4 /mnt/u
  sleep 1

  cd /mnt/u
  tar xvf /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix}/${ddisk}4.tar
  sync
   info "Default User database Created...unmounting Drive"
  sleep .5
  cd /
  umount /mnt/u

  # copy the encrypted partition to out flash
  info "Loading Core system image... this may take awhile..."
  sleep .5
  ddrescue --force /mnt/nfs/innacall/system_install_${lodtype}_${skind}_${versuffix}/${ddisk}2.img /dev/${ddisk}2
  sync

  if [ "$skind" != "soekris" ];
  then
	  # Create the SWAP file (MAXX only)
	  info "Creating Swap Partition format..."
	  mkswap /dev/${ddisk}3
	  info "System Software Installation Completed..."
  fi
fi
sleep 1
info "Mounting Boot File system for Licensing..."
sleep .5
mount /dev/${ddisk}1 /mnt/b
sleep 1
if [ "$skind" == "maxx" ];
then
	# generate the License keys or copy over existing ones.
	/mnt/nfs/innacall/initramtools/license_install_maxx.php $versuffix ${lodtype}_
	sleep 2
elif [ "$skind" == "soekris" ];
then
       	# generate the License keys or copy over existing ones.
       	/mnt/nfs/innacall/initramtools/license_install_soekris.php $versuffix ${lodtype}_
       	sleep 2
fi

info "Unmounting Boot filesystem..."
umount /mnt/b

## run shell for debugging
## /bin/bash
info "System Installation Completed...  Please manually Reboot"
info "System Halted after Installation!. Kernel Panic is Normal!"
exit
