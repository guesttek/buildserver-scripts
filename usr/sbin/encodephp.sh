#!/bin/bash
#####################################################
# This script is designed to automate encoding      #
# Php programs with sourceguardian. it will take    #
# rsync all php from strattos developer and will    #
# output a tar file. "php_encoded.tar". both        #
# in the /innacall/system_install/beta/packages     #
# directory.                                        #
#####################################################
##### NOTE!!!!!!!!!!!!!!! ###########
### THERE CANNOT BE ANY PHP     #####
### FILES in /root OF DEV UNIT  #####
#####################################
echo "Welcome to the innAcloud Software Deployment system"
if [ ! -e "/mnt/s/root" ]
then
  # our hda2 beta image likely not mounted. lets mount it
  echo "Mounting crypted disk image. Please have Access code handy!"
  losetup /dev/loop0 /innacall/system_install_beta/hda2.img
  cryptsetup luksOpen /dev/loop0 santa
  mount /dev/mapper/santa /mnt/s
  if [ ! -e "/mnt/s/root" ]
  then
    echo "Image failed to Mount! script terminated!"
    exit 1
  fi
else
  echo "Found Disk image mounted at /mnt/s"
fi
# make sure disk buffers are empty
sync

## lets backup our old development version source and compiled files.
# got version number
currver=`cat /innacall/system_install_beta/packages/version.txt`

## back up existing running beta system to archive directory
if [ ! -e /archive/beta-$currver ]
then
  echo "Archive Directory not found. creating /archive/$currver"
  mkdir -p /archive/beta-$currver
else
  echo "Archive Directory already exists. removing /archive/$currver"
  rm -vrf /archive/beta-$currver
fi

## we have our archive directory. lets populate it.
cd /innacall/system_install_beta
echo "Copying Beta system backup to archive directory"
rsync -avvr --progress /innacall/system_install_beta/ /archive/beta-$currver


# cp -prfv * /archive/$currver

## lets create our version file
echo "Creating new Software version ID"
/usr/sbin/makeversion.php

## copy version file to our image
cp -p /innacall/system_install_beta/packages/version.txt /mnt/s

## ask to deploy php files or not? this may be an update to other files.
echo -n "Deploy PHP files?(y/n):"
read cony
if [ "$cony" = "y" ]
then
  # if source dir exists, remove it and remake
  if [ -e "/innacall/system_install_beta/packages/source_php" ]
  then
  echo "Removing existing PHP source directory"
  rm -vrf /innacall/system_install_beta/packages/source_php
  mkdir /innacall/system_install_beta/packages/source_php
  fi

  # rsync the files from our strattos dev box
  echo "Downloading PHP files from developer Machine..."
  cd /root
  rsync -WRra --progress 172.16.1.216:'$(cd /; find / -path /root -prune -o -name *.php -print)' /innacall/system_install_beta/packages/source_php/

  # change to our directory for encoding
  cd /innacall/system_install_beta/packages/source_php

  # untar the php source code.
  # tar xvf ../php_source.tar
  ###### exit below if you just want to download source php from dev machine ########
  # exit
  ######
  # run sourceguardian across our directory.
  /usr/local/sg/bin/sourceguardian -z9 --phpversion 5.2 -p @../copyright.php -r /innacall/system_install_beta/packages/source_php/*.php

  # move our PHP source files (ext of .bak)
  for i in `find -name "*.bak"`
   do
        g=${i##./}
        f=${i%.bak}
        h=`dirname "../source_code${f#.}"`

        test -d $h || mkdir -p $h; echo "Created Directory: $h"

        cp -vrf "$g" "../source_code${f#.}"
   done

  find -name *.bak | xargs rm -vf

  # tar up our encoded php files

  if [ -e "../php_encoded.tar" ]
  then
    echo "Removing old encoded PHP tar archive"
    rm -vf ../php_encoded.tar
  fi

  find -name *.php | xargs tar -uvf ../php_encoded.tar

  ### we have created a new software version. lets create a new version file and install the new PHP packages to the beta ###

  ## deploy our new version files to the Beta image
  echo "Deploying newly encoded files"
  cd /mnt/s
  tar xvf /innacall/system_install_beta/packages/php_encoded.tar
else
  echo "Skipping deployment of PHP Packages"
fi

## leave the partition mounted?
echo -n "Script completed: Leave Beta Image mounted? (y/n):"
read conny
if [ "$conny" != "y" ]
then
  echo "Unmounting Beta image"
  sync
  cd /
  umount /mnt/s
  cryptsetup luksClose santa
  losetup -d /dev/loop0
else
  echo "Leaving Beta image Mounted"
  sync
fi
