#!/bin/bash

# first argument is command
cmd=$1
# second argument is archive / current
typ=$2
#third argument is dirs to use
dirs=$3

# are we working with archive or current
if [ "$typ" != "archive" ] && [ "$typ" != "current" ];
then
	echo "Type must be archive / current!"
	echo "Usage - mounter.sh [ mount/unmount ] [ archive/current ] [full path]"
        exit
fi
if [ "$typ" = "current" ];
then
	# set our base loop of 4 for current, otherwise 6
        baseloop=4
        endloop=5
else
        baseloop=6
        endloop=7
fi

## see if our command calls for mount or unmount

if [ "$cmd" = "mount" ];
then
	# does our dir exist?
	if [ -e $dirs ];
	then
		cd $dirs
		# are our loops in use?
		ii=$baseloop
		while [[ $ii -le $endloop ]];
		do
			uses=$(losetup -a | grep loop$ii | wc -l)
			if [ $uses -gt 0 ];
			then
				# our loop is in use we cant go on! exit
				echo "/dev/loop$ii is in use! please clear and try again"
				exit
			fi
		ii=$(( $ii + 1 ))
		done
		# we got here. still alive lets do the mount structure
		echo "losetup /dev/loop$baseloop $dirs/hda2.img"
		losetup /dev/loop$baseloop $dirs/hda2.img
		echo Un1t*l*Gruna1r3*Pr0j3ct | cryptsetup -v luksOpen /dev/loop$baseloop compare$baseloop
		mount /dev/mapper/compare$baseloop /mnt/$typ
		# base image mounted, now lets do our overlays
		# overlay our hda4
		mount --bind $dirs/hda4_files /mnt/$typ/mnt/kd/user
		# mount our boot partition overlay
		losetup /dev/loop$endloop $dirs/hda1.img
		mount /dev/loop$endloop /mnt/$typ/boot
		# should have our full mount stream
		exit
	else
		echo "Dirs does not exist!"
		exit
	fi
elif [ "$cmd" = "unmount" ];
then
	umount /mnt/$typ/mnt/kd/user
	umount /mnt/$typ/boot
	umount /mnt/$typ
	cryptsetup luksClose compare$baseloop
	losetup -d /dev/loop$endloop
	losetup -d /dev/loop$baseloop
	echo "Unmounted $typ"
else
	echo "Invalid Command!"
	exit
fi
# end command if
exit
