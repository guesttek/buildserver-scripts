#!/bin/bash
#####################################################
# This script is designed to automate encoding      #
# Php programs with sourceguardian. it will take    #
# rsync all php from strattos developer and will    #
# output a tar file. "php_encoded_v2.tar". both        #
# in the /innacall/system_install/beta/packages     #
# directory.                                        #
#####################################################
##### NOTE!!!!!!!!!!!!!!! ###########
### THERE CANNOT BE ANY PHP     #####
### FILES in /root OF DEV UNIT  #####
#####################################
echo "Welcome to the innAcloud 101 / 360 Software Deployment system"
echo -n "Enter the suffix (ie v3) of the version to encode"
read vsuffix

# is our loop device in use? if so bug out
if [ $(losetup -a | grep loop1 | wc -l) -eq 0 ];
then
  # our hda2 beta image likely not mounted. lets mount it
  echo "Mounting crypted disk image. Please have Access code handy!"
  losetup /dev/loop1 /innacall/system_install_beta_$vsuffix/hda2.img
  cryptsetup luksOpen /dev/loop1 rudolph
  mount /dev/mapper/rudolph /mnt/$vsuffix
  if [ ! -e "/mnt/$vsuffix/root" ]
  then
    echo "Image failed to Mount! script terminated!"
    exit 1
  fi
else
  echo "found loop1 device in use!!"
  losetup -a | grep loop1
#  echo "Found Disk image mounted at /mnt/$vsuffix"
  echo " Please unmount and clear the loop device before running this script!"
  exit 1
fi

# make sure disk buffers are empty
sync

## lets backup our old development version source and compiled files.
# got version number
currver=`cat /innacall/system_install_beta_$vsuffix/packages/version.txt`

## back up existing running beta system to archive directory
if [ ! -e /archive/beta-$vsuffix-$currver ]
then
  echo "Archive Directory not found. creating /archive/$vsuffix-$currver"
  mkdir -p /archive/beta-$vsuffix-$currver
else
  echo "Archive Directory already exists. removing /archive/$vsuffix-$currver"
  rm -vrf /archive/beta-$vsuffix-$currver
fi

## we have our archive directory. lets populate it.
cd /innacall/system_install_beta_$vsuffix
if [ -e /innacall/system_install_beta_$vsuffix ];
then
	cd /innacall/system_install_beta_$vsuffix
	echo "Copying Beta system backup to archive directory"
	rsync -avvr --progress /innacall/system_install_beta_$vsuffix/ /archive/beta-$vsuffix-$currver
else
  echo "New version created archive skipped!"
fi

  ## use trunk or our branch from SVN?.
  echo -n "Deploy MAXX $vsuffix files from branches?(y/n)(n uses trunk):"
  read conye
  if [ "$conye" == "n" ]
  then
        usebrancher="no"
  else
        usebrancher="yes"
  fi
echo SVN Branch deploy setting: $usebrancher

## image mounted lets create our version file
echo "Creating new Software version ID"
/usr/sbin/makeversionv2.php $vsuffix $usebrancher

## copy version file to our image
cp -p /innacall/system_install_beta_$vsuffix/packages/version.txt /mnt/$vsuffix

## ask to deploy php files or not? this may be an update to other files.
echo -n "Deploy $vsuffix PHP files?(y/n):"
read cony
if [ "$cony" = "y" ]
then
### Leave source dir in this temporary mode ###
  # if source dir exists, remove it and remake
  if [ -e "/innacall/system_install_beta_$vsuffix/packages/source_php" ]
  then
  echo "Removing existing PHP source encoding directory"
  rm -vrf /innacall/system_install_beta_$vsuffix/packages/source_php
  mkdir /innacall/system_install_beta_$vsuffix/packages/source_php
  else
  mkdir /innacall/system_install_beta_$vsuffix/packages/source_php
  fi

  if [ "$usebrancher" = "yes" ];
  then
          echo "Copying PHP files from SVN BRANCHES"
          cd /innacall/svn_iact50/branches/$(echo -n $vsuffix | tail -c 1).0
  else
          echo "Copying PHP files from SVN TRUNK"
          cd /innacall/svn_iact570/trunk
  fi

  # we arent going to update the SVN from this script. it should be at the revision we want to use.
#  svnrevision=`svnversion`
#  mainversion=`cat /innacall/system_install_beta_v2/packages/version.txt`
  # update version file.
#  echo "$mainversion-$svnrevision" > /innacall/system_install_beta_v2/packages/version.txt
  ## copy version file to our image
#  cp -p /innacall/system_install_beta_v2/packages/version.txt /mnt/v2
#  echo "Using SVN Revision: $svnrevision"
#  echo "Full software Version: $mainversion-$svnrevision"
#  rsync -WRra --progress /innacall/svn_iact50/'$(cd /; find / -path /root -prune -o -name *.php -print)' /innacall/system_install_beta_v2/packages/source_php/
  ## grab current encoded php files from regular system beta. only encode them once.
#  cp -prfv /innacall/system_install_beta/packages/source_code/* /innacall/system_install_beta_v2/packages/source_php
  # change to our directory for encoding
find ./ -name "*.php" -exec rsync -avvR {} /innacall/system_install_beta_$vsuffix/packages/source_php/ \;
cd /innacall/system_install_beta_$vsuffix/packages/source_php

  # untar the php source code.
  # tar xvf ../php_source.tar
  ###### exit below if you just want to download source php from dev machine ########
  # exit
  ######

  # run sourceguardian across our directory for php 5.2 stuff.
#  /usr/local/sg/bin/sourceguardian -z9 --phpversion 5.2 -p @../copyright.php -r --exclude "log-client.php" --exclude "iact_client.php" --exclude "testch.php" /innacall/system_install_beta_v2/packages/source_php/*.php
  # run sourceguardian across our directory for php 5.3 stuff.
#  /usr/local/sg/bin/sourceguardian -z9 --phpversion 5.3 -p @../copyright.php usr/sbin/log-client.php usr/sbin/iact_client.php usr/sbin/testch.php

  # run sourceguardian across our directory for php 5.3 stuff.
  /usr/local/sg/bin/sourceguardian -z9 --phpversion 5.3 -p @../copyright.php -r --exclude "/innacall/system_install_beta_$vsuffix/packages/source_php/mnt/kd/www/phpagi-phones/*" --exclude "common_routines-phones.php" --exclude "common_routines-web.php" --exclude "booter.php" --exclude "ack911.php" --exclude "autoatt.php" --exclude "coverbutton.php" --exclude "getlights.php" --exclude "newphone.php" --exclude "reports.php" --exclude "roomblock.php" --exclude "roomops.php" --exclude "/innacall/system_install_beta_$vsuffix/packages/source_php/mnt/kd/www/*.php" --exclude "/innacall/system_install_beta_$vsuffix/packages/source_php/mnt/kd/www/plugins/*.php" /innacall/system_install_beta_$vsuffix/packages/source_php/*.php
  # run sourceguardian across our directory for php 5.2 stuff.
  /usr/local/sg/bin/sourceguardian -z9 --phpversion 5.2 -p @../copyright.php mnt/kd/www/phpagi-phones/*.php usr/sbin/common_routines-phones.php usr/sbin/common_routines-web.php boot/booter.php mnt/kd/www/aastra/ack911.php  mnt/kd/www/aastra/autoatt.php   mnt/kd/www/aastra/coverbutton.php  mnt/kd/www/aastra/getlights.php  mnt/kd/www/aastra/newphone.php  mnt/kd/www/aastra/reports.php  mnt/kd/www/aastra/roomblock.php  mnt/kd/www/aastra/roomops.php mnt/kd/www/*.php mnt/kd/www/plugins/*.php

  # move our PHP source files (ext of .bak)
  for i in `find -name "*.bak"`
   do
        g=${i##./}
        f=${i%.bak}
        h=`dirname "../source_code${f#.}"`

        test -d $h || mkdir -p $h; echo "Created Directory: $h"

        cp -pvrf "$g" "../source_code${f#.}"

   done
## leave client stuff unencoded for now its different.
# cp -prfv ../source_code/opt/cloud9/bin/*.php opt/cloud9/bin/
#  find -name *.bak | xargs rm -vf

  # tar up our encoded php files

  if [ -e "../php_encoded_$vsuffix.tar" ]
  then
    echo "Removing old encoded PHP tar archive"
    rm -vf ../php_encoded_$vsuffix.tar
  fi

  find -name *.php | xargs tar -uvf ../php_encoded_$vsuffix.tar

  ### we have created a new software version. lets create a new version file and install the new PHP packages to the beta ###

  ## deploy our new version files to the Beta image
  echo "Deploying newly encoded files"
  cd /mnt/$vsuffix
  tar xvf /innacall/system_install_beta_$vsuffix/packages/php_encoded_$vsuffix.tar
  ## add version files to the encoded php files tar
  tar -uvf /innacall/system_install_beta_$vsuffix/packages/php_encoded_$vsuffix.tar version.txt
  tar -uvf /innacall/system_install_beta_$vsuffix/packages/php_encoded_$vsuffix.tar etc/motd
  tar -uvf /innacall/system_install_beta_$vsuffix/packages/php_encoded_$vsuffix.tar etc/issue
else
  echo "Skipping deployment of PHP Packages"
fi

## should we deploy other files (Non PHP) from SVN to the image?
echo -n "Deploy Non PHP files from SVN to the image?"
read conny
if [ "$conny" = "y" ];
then
  if [ "$usebrancher" = "yes" ];
  then
          echo "Copying files from SVN BRANCHES"
          rsync --progress -av --exclude "*.php" --exclude ".svn" /innacall/svn_iact570/branches/$(echo -n $vsuffix | tail -c 1).0/ /mnt/$vsuffix/
  else
          echo "Copying files from SVN TRUNK"
          rsync --progress -av --exclude "*.php" --exclude ".svn" /innacall/svn_iact570/trunk/ /mnt/$vsuffix/
  fi
fi
echo "Unmounting Beta $vsuffix image"
sync
cd /
umount /mnt/v2
cryptsetup luksClose rudolph
losetup -d /dev/loop1
