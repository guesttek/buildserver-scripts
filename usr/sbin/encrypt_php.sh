#!/bin/bash

text_bold=$(tput bold)
text_special=$(tput bold; tput setaf 0; tput setab 7;)
text_normal=$(tput sgr0)

function usage() {
    echo 
    echo "usage:"

    echo "${text_bold}$0${text_normal} [options] ${text_bold}-i=<input_file>${text_normal} ${text_bold}-o=<output_dir>${text_normal}"
    echo
    echo "${text_bold}-b=<btyp>${text_normal}"
    echo "${text_bold}--btype=<btyp>${text_normal}        Set the btyp"
    echo "${text_bold}-s=<systype>${text_normal}"
    echo "${text_bold}--systype=<systype> ${text_normal}  Set the systype"
    echo "${text_bold}-v=<vsuffix>${text_normal}"
    echo "${text_bold}--vsuffix=<vsuffix>${text_normal}   Set the vsuffix"
    echo
}

function error() {
    echo
    echo "${text_special}$1${text_normal}"

    if [ ! -z "$3" ]; then
        usage
    fi

    exit $2
}

btyp="release"
systype="maxx"
vsuffix="v6"

in_file=""
out_dir=""

#command line parameters
for i in "$@"; do
    case $i in
        -b=*|-btyp=*|--btyp=*)
            btyp="${i#*=}"
            shift # past argument=value
            ;;
        -s=*|-systype=*|--systype=*)
            systype="${i#*=}"
            shift # past argument=value
            ;;
        -v=*|-vsuffix=*|--vsuffix=*)
            vsuffix="${i#*=}"
            shift # past argument=value
            ;;
        -i=*|-in=*|--in=*)
            in_file="${i#*=}"
            shift # past argument=value
            ;;
        -o=*|-out=*|--out=*)
            out_dir="${i#*=}"
            shift # past argument=value
            ;;
        -h|-help|--help)
            usage
            exit 0
            ;;
        -*|--*)
            echo "Unknown option $i"
            echo 
            usage
            exit 1
            ;;
        *)
            ;;
    esac
done

packages_dir="/innacall/system_install_${btyp}_${systype}_${vsuffix}/packages"
source_php_dir="${packages_dir}/source_php"
in_dir=$(dirname "${in_file}")
in_file_basename=$(basename "${in_file}")
out_file="${out_dir}/${in_file_basename}"

if [ -z "${in_file}" ]; then
    error "ERROR: input file not set" 1 "show_usage"
fi
if [ -z "${out_dir}" ]; then
    error "ERROR: output dir not set" 2 "show_usage"
fi
if [ ! -f "${in_file}" ]; then
    error "ERROR: could not find input file '${in_file}'" 3
fi
if [ ! -d "${out_dir}" ]; then
    error "ERROR: could not find output dir '${out_dir}'" 4
fi
if [ -f "${out_file}" ]; then
    echo "WARNING: output file '${out_file}' already exists"
    read -p "         Do you want to overwrite it? ${text_special}y${text_normal}es / ${text_special}n${text_normal}o: " file_exist_answer
    file_exist_answer=${file_exist_answer:-n}

    if [ "$file_exist_answer" != "y" ]; then
        echo "Skipped"
        exit 23
    fi
fi


echo "encrypting"
echo "  in : ${in_file}"
echo "  out: ${out_file}"

(cd "$in_dir" && /usr/local/sourceguardian/bin/sourceguardian -o "${out_dir}" -z9 --phpversion 5.3 -p @${packages_dir}/copyright.php -r ${in_file_basename})

if [ "$?" == "0" ]; then
	echo "    ok"
else
    echo "    FAIL"
fi

echo
