#!/usr/bin/php
<?php
#################################################################
# this program is used to update version numbers of software    #
#################################################################
# 030911 - 1100am - fixed issue where copyright file was not version updated
#

$current_version = trim(file_get_contents("/innacall/system_install_beta/packages/version.txt"));
$current_version_array = explode(".",$current_version);
echo "Current Major release: $current_version_array[0]\n";
echo "Current Minor release: $current_version_array[1]\n";
echo "Current Trivial release: $current_version_array[2]\n";
echo "Current Update release: $current_version_array[3]\n";
function make_version() {
global $current_version_array;

$new_version_array[0] = readline(" Enter new major version :$current_version_array[0]: ");
if ($new_version_array[0] == "") {
  $new_version_array[0] = $current_version_array[0];
}

$new_version_array[1] = readline(" Enter new minor version :$current_version_array[1]: ");
if ($new_version_array[1] == "") {
  $new_version_array[1] = $current_version_array[1];
}

$new_version_array[2] = readline(" Enter new trivial version :$current_version_array[2]: ");
if ($new_version_array[2] == "") {
  $new_version_array[2] = $current_version_array[2];
}

$new_version_array[3] = readline(" Enter new minor version :$current_version_array[3]: ");
if ($new_version_array[3] == "") {
  $new_version_array[3] = $current_version_array[3];
}
$version = NULL;
foreach ($new_version_array as $item) {
  $version .= $item.".";
}
# strip last . off
$version = substr($version,0,-1);
## get our svn version
exec("cd /innacall/svn_iact570/branches/2.0; svnversion",$svn_suffix);
$version .= "-".trim($svn_suffix[0]);
return $version;
}

$newversion = make_version();
$rr = readline ("New software version $newversion : is this correct?");
if ($rr == "Y" || $rr == "y") {
  # write new version file
  $fh = fopen("/innacall/system_install_beta/packages/version_temp.txt",'w+');
  if ($fh) {
    # write out actual version file
    fwrite($fh,$newversion);
    fclose ($fh);
    rename("/innacall/system_install_beta/packages/version_temp.txt","/innacall/system_install_beta/packages/version.txt");    
    # create the copyright file
    $cright_old = file_get_contents("/innacall/system_install_beta/packages/copyright-template.txt");
    $cright_new = str_replace("rversionr",$newversion,$cright_old);
    $fi = fopen("/innacall/system_install_beta/packages/copytemp.txt",'w+');
    fwrite($fi,$cright_new);
    fclose($fi);
    rename("/innacall/system_install_beta/packages/copytemp.txt","/innacall/system_install_beta/packages/copyright.php");    
    # write out the issue file
    $issue_old = file_get_contents("/innacall/system_install_beta/packages/issue_temp.txt");
    $issue_new = str_replace(":vvvr:",$newversion,$issue_old);
    $fj = fopen("/innacall/system_install_beta/packages/issuetemp.txt",'w+');
    fwrite($fj,$issue_new);
    fclose($fj);    
    rename("/innacall/system_install_beta/packages/issuetemp.txt","/mnt/s/etc/issue");
    $fk = fopen("/innacall/system_install_beta/packages/motdtemp.txt",'w+');
    fwrite($fk,"innAcloud strattos version: $newversion\n");
    fclose($fk);    
    rename("/innacall/system_install_beta/packages/motdtemp.txt","/mnt/s/etc/motd");    
  } else {
    echo "Unable to create new version file.. system version not changed!\n";
  }
} else {
$newversion = make_version();       
}

?>