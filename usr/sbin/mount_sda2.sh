#!/bin/bash

losetup /dev/loop4 /innacall/system_install_release_maxx_v6/sda2.img
echo Un1t*l*Gruna1r3*Pr0j3ct | cryptsetup -v luksOpen /dev/loop4 compare4
mount /dev/mapper/compare4 /mnt/current

mount --bind /innacall/system_install_release_maxx_v6/sda4_files /mnt/current/mnt/kd/user
