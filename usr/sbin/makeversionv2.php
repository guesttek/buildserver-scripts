#!/usr/bin/php
<?php
#################################################################
# this program is used to update version numbers of software    #
#################################################################
# 030911 - 1100am - fixed issue where copyright file was not version updated
#
if (array_key_exists(1,$argv) && is_numeric(substr($argv[1],-1))) {
  $vsuffix = trim($argv[1]);
} else {
  echo "FATAL!!  argument must end with a number!\n";
}

if (array_key_exists(2,$argv)) {
  $usebranch = $argv[2];
} else {
  echo "No Build selector sent, using 'Branch'\n";
  $usebranch = "yes";
}

$current_version = trim(file_get_contents("/innacall/system_install_beta_$vsuffix/packages/version.txt"));
$current_version_array = explode(".",$current_version);
echo "Current Major release: $current_version_array[0]\n";
echo "Current Minor release: $current_version_array[1]\n";
echo "Current Trivial release: $current_version_array[2]\n";
echo "Current Update release: $current_version_array[3]\n";
function make_version() {
global $vsufnum, $vsuffix, $current_version_array, $usebranch;

$new_version_array[0] = readline(" Enter new major version :$current_version_array[0]: ");
if ($new_version_array[0] == "") {
  $new_version_array[0] = $current_version_array[0];
}

$new_version_array[1] = readline(" Enter new minor version :$current_version_array[1]: ");
if ($new_version_array[1] == "") {
  $new_version_array[1] = $current_version_array[1];
}

$new_version_array[2] = readline(" Enter new trivial version :$current_version_array[2]: ");
if ($new_version_array[2] == "") {
  $new_version_array[2] = $current_version_array[2];
}

$new_version_array[3] = readline(" Enter new minor version :$current_version_array[3]: ");
if ($new_version_array[3] == "") {
  $new_version_array[3] = $current_version_array[3];
}
$version = NULL;
foreach ($new_version_array as $item) {
  $version .= $item.".";
}
# strip last . off
$version = substr($version,0,-1);
## get our svn version
var_dump($usebranch);
if ($usebranch == "no") {
  ## use trunk
  echo "Using SVN Trunk\n";
  exec("cd /innacall/svn_iact570/trunk; svnversion",$svn_suffix);
} else {
  $vsufnum = substr($vsuffix,-1);
  echo "Using SVN Branch\n";
  exec("cd /innacall/svn_iact570/branches/$vsufnum.0; svnversion",$svn_suffix);
}
$version .= "-".trim($svn_suffix[0]);
return $version;
}
$rr = NULL;
while ( $rr != "y" && $rr != "Y") {
  $newversion = make_version();
  $rr = readline ("New software version $newversion : is this correct?");
}
#if ($rr == "Y" || $rr == "y") {
  # write new version file
  $fh = fopen("/innacall/system_install_beta_$vsuffix/packages/version_temp.txt",'w+');
  if ($fh) {
    # write out actual version file
    fwrite($fh,$newversion);
    fclose ($fh);
    rename("/innacall/system_install_beta_$vsuffix/packages/version_temp.txt","/innacall/system_install_beta_$vsuffix/packages/version.txt");    
    # create the copyright file
    $cright_old = file_get_contents("/innacall/system_install_beta_$vsuffix/packages/copyright-template.txt");
    $cright_new = str_replace("rversionr",$newversion,$cright_old);
    $fi = fopen("/innacall/system_install_beta_$vsuffix/packages/copytemp.txt",'w+');
    fwrite($fi,$cright_new);
    fclose($fi);
    rename("/innacall/system_install_beta_$vsuffix/packages/copytemp.txt","/innacall/system_install_beta_$vsuffix/packages/copyright.php");    
    # write out the issue file
    $issue_old = file_get_contents("/innacall/system_install_beta_$vsuffix/packages/issue_temp.txt");
    $issue_new = str_replace(":vvvr:",$newversion,$issue_old);
    $fj = fopen("/innacall/system_install_beta_$vsuffix/packages/issuetemp.txt",'w+');
    fwrite($fj,$issue_new);
    fclose($fj);
    rename("/innacall/system_install_beta_$vsuffix/packages/issuetemp.txt","/mnt/$vsuffix/etc/issue");
    $fk = fopen("/innacall/system_install_beta_$vsuffix/packages/motdtemp.txt",'w+');
    fwrite($fk,"innAcloud strattos version: $newversion\n");
    fclose($fk);
    rename("/innacall/system_install_beta_$vsuffix/packages/motdtemp.txt","/mnt/$vsuffix/etc/motd");    
  } else {
    echo "Unable to create new version file.. system version not changed!\n";
  }
#} else {
#$newversion = make_version();
#}

?>
