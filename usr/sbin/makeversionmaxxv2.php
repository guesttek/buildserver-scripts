#!/usr/bin/php
<?php
#################################################################
# this program is used to update version numbers of software    #
#################################################################
# 030911 - 1100am - fixed issue where copyright file was not version updated
# /usr/sbin/makeversionmaxxv2.php ${vsuffix} ${usebrancher} ${btyp} ${systype}
#
if (array_key_exists(1,$argv) && is_numeric(substr($argv[1],-1))) {
  $vsuffix = trim($argv[1]);
} else {
  echo "FATAL!!  argument must end with a number!\n";
}
$prever = "beta";
if (array_key_exists(2,$argv)) {
  $usebranch = $argv[2];
} else {
  echo "No Build selector sent, using 'Branch'\n";
  $usebranch = "b";
}

if (array_key_exists(3,$argv)) {
  $prever = $argv[3];
} else {
  echo "No Build type sent. using Beta";
  $prever = "beta";
}


if (array_key_exists(4,$argv)) {
  $systype = $argv[4];
} else {
  echo "No SYSTEM type sent. using maxx";
  $systype = "maxx";
}

$current_version = trim(file_get_contents("/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/version.txt"));
$current_version_array = explode(".",$current_version);
echo "Current Major release: $current_version_array[0]\n";
echo "Current Minor release: $current_version_array[1]\n";
echo "Current Trivial release: $current_version_array[2]\n";
echo "Current Update release: $current_version_array[3]\n";

function make_version() {
global $vsufnum, $vsuffix, $current_version_array, $usebranch, $prever, $systype;

$new_version_array[0] = readline(" Enter new major version :$current_version_array[0]: ");
if ($new_version_array[0] == "") {
  $new_version_array[0] = $current_version_array[0];
}

$new_version_array[1] = readline(" Enter new minor version :$current_version_array[1]: ");
if ($new_version_array[1] == "") {
  $new_version_array[1] = $current_version_array[1];
}

$new_version_array[2] = readline(" Enter new trivial version :$current_version_array[2]: ");
if ($new_version_array[2] == "") {
  $new_version_array[2] = $current_version_array[2];
}

$new_version_array[3] = readline(" Enter new minor version :$current_version_array[3]: ");
if ($new_version_array[3] == "") {
  $new_version_array[3] = $current_version_array[3];
}
## new 06-24-18!!
## we want to better identify how our systems are built so we add some more identifiers in version
## numbering scheme to help us out.
//$version = $systype."-".$prever."-".$usebranch."-".$vsuffix."-";
//$version = $vsuffix."-";
$version = NULL;
foreach ($new_version_array as $item) {
  $version .= $item.".";
}
# strip last . off
$version = substr($version,0,-1);

/*
## get our svn version
var_dump($usebranch);
if (strlen($usebranch) > 1) {
  ## use custom
  echo "Using SVN Custom $usebranch\n";
  exec("cd /innacall/svn_iact-".$systype."/$usebranch; svnversion",$svn_suffix);
} elseif ($usebranch == "t") {
  ## use trunk
  echo "Using SVN Trunk\n";
  exec("cd /innacall/svn_iact-".$systype."/trunk; svnversion",$svn_suffix);
} elseif ($usebranch == "a") {
  ## use alpha
  exec("cd /innacall/svn_iact-".$systype."/alpha; svnversion",$svn_suffix);
} elseif ($usebranch == "r") {
  ## use trunk
  echo "Using SVN RC\n";
  exec("cd /innacall/svn_iact-".$systype."/rc; svnversion",$svn_suffix);
} else {
  $vsufnum = substr($vsuffix,-1);
  echo "Using SVN Branch\n";
  exec("cd /innacall/svn_iact-".$systype."/branches/$vsufnum.0; svnversion",$svn_suffix);
}
*/

$buildversion = readline(" Enter new build version:");
print_r ($buildversion);
//$version .= "-".trim($svn_suffix[0]);
$version .= "-".$buildversion;
return $version;
}
$rr = NULL;
while ($rr != "Y" && $rr != "y") {
  $rr = NULL;
  $newversion = NULL;
  $newversion = make_version();
  $rr = readline ("New software version $newversion : is this correct?");
}
  # write new version file
  $fh = fopen("/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/version_temp.txt",'w+');
  if ($fh) {
    # write out actual version file
    fwrite($fh,$newversion."\n");
    fclose ($fh);
    rename("/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/version_temp.txt","/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/version.txt");
    # create the copyright file
    $cright_old = file_get_contents("/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/copyright-template.txt");
    $cright_new = str_replace("rversionr",$newversion,$cright_old);
    $fi = fopen("/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/copytemp.txt",'w+');
    fwrite($fi,$cright_new);
    fclose($fi);
    rename("/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/copytemp.txt","/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/copyright.php");
    # write out the issue file
    $issue_old = file_get_contents("/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/issue_temp.txt");
    $issue_new = str_replace(":vvvr:",$newversion,$issue_old);
    $fj = fopen("/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/issuetemp.txt",'w+');
    fwrite($fj,$issue_new);
    fclose($fj);
    rename("/innacall/system_install_".$prever."_".$systype."_$vsuffix/packages/issuetemp.txt","/mnt/".$systype."$vsuffix/etc/issue");
    $fk = fopen("/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/motdtemp.txt",'w+');
    fwrite($fk,"OneView Voice - $systype version: $newversion\n");
    fclose($fk);
    rename("/innacall/system_install_".$prever."_".$systype."_".$vsuffix."/packages/motdtemp.txt","/mnt/".$systype."$vsuffix/etc/motd");
  } else {
    echo "Unable to create new version file.. system version not changed!\n";
  }

?>
