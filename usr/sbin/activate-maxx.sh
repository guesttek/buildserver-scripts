#!/bin/bash
###########################################################################
# this script is designed to make a beta configuration active as          #
# a real system release version of software. use with caution!            #
# a backup of the current release directory will be made under /archives  #
###########################################################################
## get our "v number"
echo -n "Please enter the MAXX beta version suffix. (ie. v3):"
read vsuffix
## see if user really wants to proceed?
currver=`cat /innacall/system_install_maxx/version.txt`
newver=`cat /innacall/system_install_beta_maxx_$vsuffix/packages/version.txt`

echo -n "You are upgrading active load from :$currver: to :$newver: are you sure?(y/n)"
read conny
if [ "$conny" != "y" ]
then
  exit
fi

## make sure the beta load partition is not mounted.
if [ -e "/mnt/maxx$vsuffix/root" ]
then
  echo "Unmounting System beta image partition"
  sync
  umount /mnt/maxx$vsuffix
  cryptsetup luksClose donner
  losetup -d /dev/loop2
fi

if [ -e "/mnt/t/root" ]
then
  echo "Unmounting Running System image partition"
  sync
  umount /mnt/t
  cryptsetup luksClose santa2
  losetup -d /dev/loop1
fi

## back up existing running system to archive directory
if [ ! -e /archive/$currver ]
then
  echo "Archive Directory not found. creating /archive/$currver"
  mkdir -p /archive/$currver
else
  echo "Archive Directory already exists. removing /archive/$currver"
  rm -vrf /archive/$currver
fi

## we have our archive directory. lets populate it.
cd /innacall/system_install_maxx
echo "Copying system backup to archive directory"
rsync -avvr --progress /innacall/system_install_maxx/ /archive/$currver


# cp -prfv * /archive/$currver

## archive made. lets copy over our install dir
cd /innacall/system_install_beta_maxx_$vsuffix
echo "Creating System Install deploy files"
rsync -avvr --progress sda1_files/ /innacall/system_install_maxx/sda1_files/
rsync -avv --progress *.img /innacall/system_install_maxx/
rsync -avv --progress sda4.tar /innacall/system_install_maxx/
rsync -avv --progress *.txt /innacall/system_install_maxx/
rsync -avvr --progress keyed_files/ /innacall/system_install_maxx/keyed_files/
rsync -avv --progress packages/version.txt /innacall/system_install_maxx/

echo "System upgrade deployed. test installation!"
 