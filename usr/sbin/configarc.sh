#!/bin/bash
## use this script to back up config sets to the archive.
## it takes the tar size and date to make the signature

ARG=$1
if [ ${#ARG} -eq 0 ];
then
	echo "Usage configarc.sh [configname]"
	exit
fi

if [ ! -d /innacall/configs/${ARG} ];
then
	echo FATAL! Config directory /innacall/configs/${ARG} Not found!
	exit
fi
cd /innacall/configs

testar=$(tar -tf ${ARG}/configs.tar > /dev/null 2>&1 ; echo $?)

if [ $testar -ne 0 ];
then
	echo "FATAL! Config file tar not Valid!"
	exit
fi

## passed our mini validations lets create a copy

# create version ID
s=$(ls -alt --full-time ${ARG}/configs.tar | awk '{print $6,"_",$7}')

# make a copy to /archive/configs

cp -prfv ${ARG} "/archive-v6/configs/${ARG}_${s//[[:blank:]]/}"


