#!/bin/bash

# first argument is command
cmd=$1
# second argument is archive / current
typ=$2
#third argument is dirs to use
dirs=$3
# system type light / maxx
stypo=$4

# configset
confset=$5

# are we working with archive or current
if [ "$typ" != "archive" ] && [ "$typ" != "current" ];
then
	echo "Type must be archive / current!"
	echo "Usage - mounter.sh [ mount/unmount ] [ archive/current ] [full path] [light  / maxx] [configname]"
        exit
fi
if [ "$typ" = "current" ];
then
	# set our base loop of 4 for current, otherwise 6
        baseloop=4
        endloop=5
else
        baseloop=6
        endloop=7
fi

## see if our command calls for mount or unmount

if [ "$cmd" = "mount" ];
then
	## are we working with light or maxx. sets hda vs sda flag
	if [ "$stypo" = "maxx" ];
	then
	        dtyp="s"
	elif [ "$stypo" = "light" ];
	then
	        dtyp="h"
	else
        	echo "System load type must be light or maxx"
        	exit
	fi
	# does our dir exist?
	if [ -e $dirs ];
	then
		cd $dirs
		# are our loops in use?
		ii=$baseloop
		while [[ $ii -le $endloop ]];
		do
			uses=$(losetup -a | grep loop$ii | wc -l)
			if [ $uses -gt 0 ];
			then
				# our loop is in use we cant go on! exit
				echo "/dev/loop$ii is in use! please clear and try again"
				exit
			fi
		ii=$(( $ii + 1 ))
		done
		# we got here. still alive lets do the mount structure
		echo "losetup /dev/loop$baseloop $dirs/${dtyp}da2.img"
		losetup /dev/loop$baseloop $dirs/${dtyp}da2.img
		echo Un1t*l*Gruna1r3*Pr0j3ct | cryptsetup -v luksOpen /dev/loop$baseloop compare$baseloop
		mount /dev/mapper/compare$baseloop /mnt/$typ
		# base image mounted, now lets do our overlays
		# overlay our config set
               	# if we have a / in our configset we take it literally otherwise
               	# we assume the standard configs directory
		if [[ ${confset} == *"/"* ]];
		then
			conftoload=${confset}/config_files
		elif [ $confset = default ];
		then
               	        # default config
                       	conftoload=$dirs/${dtyp}da4_files
		else
                       	conftoload=/innacall/configs/${confset}/config_files
		fi

		if [ -e ${conftoload} ];
		then
			# if we have a / in our configset we take it literally otherwise
			# we assume the standard configs directory

			mount --bind ${conftoload} /mnt/$typ/mnt/kd/user
#		elif [ $confset = default ];
#		then
#			mount --bind $dirs/${dtyp}da4_files /mnt/$typ/mnt/kd/user
		else
			echo "Config set: ${confset} does not Exist!"
		fi
# old
#		mount --bind $dirs/${dtyp}da4_files /mnt/$typ/mnt/kd/user
#
		# mount our boot partition overlay
		losetup /dev/loop$endloop $dirs/${dtyp}da1.img
		mount /dev/loop$endloop /mnt/$typ/boot
		# should have our full mount stream
		exit
	else
		echo "Dirs does not exist!"
		exit
	fi
elif [ "$cmd" = "unmount" ];
then
	umount /mnt/$typ/mnt/kd/user
	umount /mnt/$typ/boot
	umount /mnt/$typ
	cryptsetup luksClose compare$baseloop
	losetup -d /dev/loop$endloop
	losetup -d /dev/loop$baseloop
	echo "Unmounted $typ"
else
	echo "Invalid Command!"
	exit
fi
# end command if
exit
