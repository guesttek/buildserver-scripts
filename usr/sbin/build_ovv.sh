#!/bin/bash
#####################################################
# This script is designed to automate encoding      #
# Php programs with sourceguardian. it will take    #
# rsync all php from strattos developer and will    #
# output a tar file. "php_encoded_maxx.tar". both   #
# in the /innacall/system_install/beta/packages     #
# directory.                                        #
#####################################################
##### NOTE!!!!!!!!!!!!!!! ###########
### THERE CANNOT BE ANY PHP     #####
### FILES in /root OF SVN REPO  #####
#####################################
echo "Welcome to the OVV Appliance Software Deployment system"
echo -n "Enter the system type you are Building. (soekris/maxx)"
read systype

echo -n "Enter the suffix (ie v3) of the version to encode"
read vsuffix

echo -n "Enter the build you are encoding. (alpha,beta,release,rc or <NAME>)"
read btyp

if [ ! -e "/innacall/system_install_${btyp}_${systype}_${vsuffix}" ];
then
	echo /innacall/system_install_${btyp}_${systype}_$vsuffix does not exist
	exit 1
fi

# new sourceguardian version available. allow the user to select it if they wish
echo "NEW!! parameter! Valid values are 8 - Default, or 10 - New version (experimental)"
echo -n "Enter the SourceGuardian to use: "
read sgversion

if [ $sgversion -eq 10 ];
then
	sgdir="sourceguardian"
	echo "Sourceguardian Version 10 Selected!"
else
	sgdir="sg"
	echo "Sourceguardian Version 8 Selected!"
fi
 

# set disk image based on system type
if [ "$systype" = "soekris" ];
then
	dimage="hda2"
	bimage="hda1"
else
	dimage="sda2"
	bimage="sda1"
fi

# create the Dir to mount on if it doesnt exist
if [ ! -e /mnt/${systype}${vsuffix} ];
then
	mkdir -v /mnt/${systype}${vsuffix}
fi 

if [ $(losetup -a | grep loop2 | wc -l) -eq 0 ];
# if [ ! -e "/mnt/${systype}${vsuffix}/root" ]
then
  # our sda2 beta image likely not mounted. lets mount it
  echo "Mounting crypted disk image."
  losetup /dev/loop2 /innacall/system_install_${btyp}_${systype}_${vsuffix}/${dimage}.img
  # cryptsetup luksOpen /dev/loop2 donner
  echo Un1t*l*Gruna1r3*Pr0j3ct | cryptsetup luksOpen /dev/loop2 donner
  mount /dev/mapper/donner /mnt/${systype}${vsuffix}
  if [ ! -e "/mnt/${systype}${vsuffix}/root" ]
  then
    echo "Image failed to Mount! script terminated!"
    exit 1
  fi
  # mount our boot partition up
  mount /innacall/system_install_${btyp}_${systype}_${vsuffix}/${bimage}.img /mnt/${systype}${vsuffix}/boot
else
  echo "found loop2 device in use!!" 
  losetup -a | grep loop2
#  echo "Found Disk image mounted at /mnt/${systype}${vsuffix}"
  echo " Please unmount and clear the loop device before running this script!"
  exit 1
fi

# make sure disk buffers are empty
sync

## lets backup our old development version source and compiled files.
# got version number
## NEW 06-24-18!!! currver data is included in version txt  except for first run.
#currver=`cat /innacall/system_install_${btyp}_${systype}_${vsuffix}/packages/version.txt`

## back up existing running beta system to archive directory
#if [ ! -e /archive-$vsuffix/$currver ];
# if [ ! -e /archive-$vsuffix/${systype}-${btyp}-$vsuffix-$currver ]
#then
#  echo "Archive Directory not found. creating /archive/${systype}-${btyp}-$vsuffix-$currver"
#echo "Archive Directory not found. creating /archive-$vsuffix/$currver"
#  mkdir -p /archive-$vsuffix/${systype}-${btyp}-$vsuffix-$currver
#mkdir -p /archive-$vsuffix/$currver
#else
#  echo "Archive Directory already exists. removing /archive/$currver"
#  rm -vrf /archive-$vsuffix/${systype}-${btyp}-$vsuffix-$currver
#rm -vrf /archive-$vsuffix/$currver
#fi

## we have our archive directory. lets populate it.
#cd /innacall/system_install_${btyp}_${systype}_${vsuffix}
#if [ -e /innacall/system_install_${btyp}_${systype}_${vsuffix} ];
#then
#  cd /innacall/system_install_${btyp}_${systype}_${vsuffix}
#  echo "Copying ${btyp} system backup to archive directory"
#  rsync -avvr --progress /innacall/system_install_${btyp}_${systype}_${vsuffix}/ /archive-$vsuffix/${systype}-${btyp}-$vsuffix-$currver
#  rsync -avvr --progress /innacall/system_install_${btyp}_${systype}_${vsuffix}/ /archive-$vsuffix/$currver
#  touch /archive-$vsuffix/${systype}-${btyp}-$vsuffix-$currver
#  touch /archive-$vsuffix/$currver
#else
#  echo "New version created archive skipped!"
#fi

goodi=0

while [ $goodi -eq 0 ];
do
  ## use trunk or our branch from SVN?.
  echo -n "Deploy ${systype} $vsuffix files from SVN alpha, branches, trunk, rc, or custom (a/b/t/r/c)?:"
  read conye

  case $conye in
  	a|b|t|r|c)
	usebrancher=$conye
	goodi=1
  	;;
  	*)
	echo "Please type a - SVN alpha, b - SVN branch, t - SVN trunk, r - SVN rc, c - SVN custom!!"
  	;;
  esac
  if [ "$conye" = "c" ];
  then
	## use trunk or our branch from SVN?.
  	echo -n "Please enter Custom SVN Repository Name:"
  	read cunye
  fi
  if [ ${#cunye} -gt 0 ];
  then
	usebrancher=$cunye
	echo "Using Custom SVN $usebrancher"
  else
	echo "Error! Must type single word SVN name"
	goodi=0
  fi
done


## image mounted lets create our version file
echo "Creating new Software version ID"
echo "Running - /usr/sbin/makeversionmaxxv2.php ${vsuffix} ${usebrancher} ${btyp} ${systype}"
/usr/sbin/makeversionmaxxv2.php ${vsuffix} ${usebrancher} ${btyp} ${systype}


##### stopped here #####


## copy version file to our image
cp -p /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/version.txt /mnt/${systype}${vsuffix}

## ask to deploy php files or not? this may be an update to other files.
echo -n "Deploy ${systype} $vsuffix PHP files?(y/n):"
read cony
if [ "$cony" = "y" ]
then
### Leave source dir in this temporary mode ###
  # if source dir exists, remove it and remake
  if [ -e "/innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php" ]
  then
  echo "Removing existing PHP source encoding directory"
  rm -vrf /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php
  mkdir /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php
  else
  mkdir /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php
  fi

  case $usebrancher in
	a)
          echo "Copying PHP files from SVN ALPHA"
          cd /innacall/svn_iact-${systype}/alpha
          ;;
	b)
	  echo "Copying PHP files from SVN BRANCHES"
	  cd /innacall/svn_iact-${systype}/branches/$(echo -n $vsuffix | tail -c 1).0
	  ;;
	t)
	  echo "Copying PHP files from SVN TRUNK"
	  cd /innacall/svn_iact-${systype}/trunk
	  ;;
	r)
      	  echo "Copying PHP files from SVN RC"
       	  cd /innacall/svn_iact-${systype}/rc
	  ;;
        *)
          echo "Copying PHP files from SVN Custom $usebrancher"
#          cd /innacall/svn_iact-${systype}/$usebrancher
          cd /innacall/builds/$usebrancher
	  ;;

  esac
pwd
echo "press a key"
read ttdd

  # we arent going to update the SVN from this script. it should be at the revision we want to use.
  #  svnrevision=`svnversion`
  # update version file.
  ## copy version file to our image
  #  echo "Using SVN Revision: $svnrevision"
  #  echo "Full software Version: $mainversion-$svnrevision"
  #  rsync -WRra --progress /innacall/svn_iact50/'$(cd /; find / -path /root -prune -o -name *.php -print)' /innacall/system_install_beta_v2/packages/source_php/
  ## grab current encoded php files from regular system beta. only encode them once.
  #  cp -prfv /innacall/system_install_beta/packages/source_code/* /innacall/system_install_beta_v2/packages/source_php

  # change to our directory for encoding
  rsync -avvR --files-from=<( find ./ -name "*.php") ./ /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php/
  # find ./ -name "*.php" -exec rsync -avvR {} "/innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php/"
  cd /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php

  ##
  ## set the booter filename we encode base on system type
  if [ "$systype" = "soekris" ];
  then
        bof=booter.php
  else
        bof=booter-maxx.php
  fi

  # untar the php source code.
  # tar xvf ../php_source.tar
  ###### exit below if you just want to download source php from dev machine ########
  # exit
  ######
  ## only sourceguardian 2 version is we are not v6
  if [ "$vsuffix" = "v6" ];
  then
	echo "Using V6 single PHP version!"
	echo "Sourceguardian Version $sgversion"
	echo "press a key"
	read ttdd
	 /usr/local/$sgdir/bin/sourceguardian -z9 --phpversion 5.3 -p @../copyright.php \
	 -r /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php/*.php
  else
     	echo "Using V4 Dual PHP version!"
        echo "Sourceguardian Version $sgversion"
        echo "press a key"
	read ttdd

  	# run sourceguardian across our directory for php 5.3 stuff.
  	/usr/local/$sgdir/bin/sourceguardian -z9 --phpversion 5.3 -p @../copyright.php \
  	-r --exclude "/innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php/mnt/kd/www/phpagi-phones/*" \
  	--exclude "common_routines-phones.php" --exclude "common_routines-web.php" --exclude "${bof}" --exclude "ack911.php" \
  	--exclude "autoatt.php" --exclude "coverbutton.php" --exclude "getlights.php" --exclude "newphone.php" --exclude "reports.php" \
  	--exclude "roomblock.php" --exclude "roomops.php" --exclude "acdlogin.php" \
  	--exclude "/innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php/mnt/kd/www/*.php" \
  	--exclude "/innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php/mnt/kd/www/plugins/*.php" \
  	/innacall/system_install_${btyp}_${systype}_$vsuffix/packages/source_php/*.php
  	##

  	# run sourceguardian across our directory for php 5.2 stuff.
        /usr/local/$sgdir/bin/sourceguardian -z9 --phpversion 5.2 -p @../copyright.php mnt/kd/www/phpagi-phones/*.php \
        usr/sbin/common_routines-phones.php usr/sbin/common_routines-web.php boot/${bof} mnt/kd/www/aastra/ack911.php  \
        mnt/kd/www/aastra/autoatt.php mnt/kd/www/aastra/acdlogin.php   mnt/kd/www/aastra/coverbutton.php  mnt/kd/www/aastra/getlights.php  \
        mnt/kd/www/aastra/newphone.php  mnt/kd/www/aastra/reports.php  mnt/kd/www/aastra/roomblock.php  mnt/kd/www/aastra/roomops.php \
        mnt/kd/www/*.php mnt/kd/www/plugins/*.php
  fi
  ##
  echo "Encoding Complete! Check for errors.."
  echo "Press a key to continue..."
  read ttdd

  # move our PHP source files (ext of .bak)
  for i in `find -name "*.bak"`
   do
        g=${i##./}
        f=${i%.bak}
        h=`dirname "../source_code${f#.}"`

        test -d $h || mkdir -p $h; echo "Created Directory: $h"

        cp -pvrf "$g" "../source_code${f#.}"
        rm -vf "$g"
   done
## leave client stuff unencoded for now its different.
# cp -prfv ../source_code/opt/cloud9/bin/*.php opt/cloud9/bin/
#  find -name *.bak | xargs rm -vf

  # tar up our encoded php files

  if [ -e "../php_encoded_${systype}${vsuffix}.tar" ]
  then
    echo "Removing old encoded PHP tar archive"
    rm -vf ../php_encoded_${systype}${vsuffix}.tar
  fi

  find -name *.php | xargs tar -uvf ../php_encoded_${systype}${vsuffix}.tar

  ### we have created a new software version. lets create a new version file and install the new PHP packages to the beta ###

  ## deploy our new version files to the Beta image
  echo "Deploying newly encoded files"
  cd /mnt/${systype}${vsuffix}
  tar xvf /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/php_encoded_${systype}${vsuffix}.tar
  ## add version files to the encoded php files tar
  tar -uvf /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/php_encoded_${systype}${vsuffix}.tar version.txt
  tar -uvf /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/php_encoded_${systype}${vsuffix}.tar etc/motd
  tar -uvf /innacall/system_install_${btyp}_${systype}_$vsuffix/packages/php_encoded_${systype}${vsuffix}.tar etc/issue
else
  echo "Skipping deployment of PHP Packages"
fi

## should we deploy other files (Non PHP) from SVN to the image?
echo -n "Deploy Non PHP files from SVN to the image?"
read conny
if [ "$conny" = "y" ];
then
  case $usebrancher in

        a)
          echo "Copying files from SVN ALPHA"
          rsync --progress -av --exclude "*.php" --exclude ".svn" /innacall/svn_iact-${systype}/alpha/ /mnt/${systype}${vsuffix}/
          ;;
  	b)
          echo "Copying files from SVN BRANCHES"
          rsync --progress -av --exclude "*.php" --exclude ".svn" /innacall/svn_iact-${systype}/branches/$(echo -n $vsuffix | tail -c 1).0/ \
	  /mnt/${systype}${vsuffix}/
	  ;;
  	t)
          echo "Copying files from SVN TRUNK"
	  rsync --progress -av --exclude "*.php" --exclude ".svn" /innacall/svn_iact-${systype}/trunk/ /mnt/${systype}${vsuffix}/
  	  ;;
  	r)
          echo "Copying files from SVN RC"
          rsync --progress -av --exclude "*.php" --exclude ".svn" /innacall/svn_iact-${systype}/rc/ /mnt/${systype}${vsuffix}/
  	  ;;
        *)
          echo "Copying Non PHP files from SVN Custom $usebrancher"
#          rsync --progress -av --exclude "*.php" --exclude ".svn" /innacall/svn_iact-${systype}/$usebrancher/ /mnt/${systype}${vsuffix}/
          rsync --progress -av --exclude "*.php" --exclude ".svn" /innacall/builds/$usebrancher/ /mnt/${systype}${vsuffix}/
          ;;
  esac
fi
## leave the partition mounted?
## must unmount as Loop device undeleted reverts the changes if not unmounted before running.
##echo -n "Script completed: Leave ${btyp} ${systype} $vsuffix Image mounted? (y/n):"
##read conny
##if [ "$conny" != "y" ]
##then
  echo "Unmounting ${btyp} ${systype} $vsuffix image"
  sync
  cd /
  umount -l /mnt/${systype}${vsuffix}/boot

  umount -l /mnt/${systype}${vsuffix}
  cryptsetup luksClose donner
  losetup -d /dev/loop2
##else
##  echo "Leaving ${btyp} ${systype} $vsuffix image Mounted"
##  sync
##fi

configs_tar="/innacall/configs/ovv/configs.tar"
echo "Removing ${configs_tar}"
rm -rf ${configs_tar}

sda4_files="/innacall/system_install_${btyp}_${systype}_${vsuffix}/sda4_files"
echo "Creating ${configs_tar} from ${sda4_files}"
tar cfP ${configs_tar} ${sda4_files}/* --transform="s:${sda4_files}::g"

echo "Showing contents of ${configs_tar}"
tar tfv ${configs_tar}
