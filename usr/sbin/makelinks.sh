#!/bin/bash

## theory of operation
# cd to the old initram and read the symlinks
cd /innacall/initramtools/test-install
# mnake a temp file with the links. so we have to find them all.
## find . -type l | xargs ls -alt > ../templinks.txt
# now we read each line of our temp file.

while read line;
do
       	## cd to the base dir of the new initram
        cd /innacall/initramtools/initram504
	## first split it by the '->'
	linesplit=$(echo $line | awk -F: '{ st = index($0,"./");print "." substr($0,st+1)}')
        ltarget=$(echo $linesplit | awk -F' -> ' '{print $2}')
	echo "--------------------"
	echo "Target: |$ltarget|"
	lname=$(echo $linesplit | awk -F' -> ' '{print $1}')
	echo "Link: |$lname|"
	ldir=$(dirname $lname)
	if [ -d $ldir ];
	then
		## cd to the dirname of the link 
		echo "cd $ldir"
		cd $ldir
		pwd
		## make our link
		echo "ln  -vsf $ltarget $(basename $lname)"
        	ln  -vsf $ltarget $(basename $lname)
	else
		echo "WARNING: Dir $ldir Not exist"
	fi
done < ../templinks.txt
