#!/bin/bash


text_bold=$(tput bold)
text_special=$(tput bold; tput setaf 0; tput setab 7;)
text_normal=$(tput sgr0)

#SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_PATH="/usr/local/bin"
INSTALLER_SCRIPTS_PATH="${SCRIPT_PATH}/build_ovv_package_scripts"
PACKAGE_ROOT="/innacall/builds/release"

OVERWRITE_ALL="n"


if [[ -z "$1" ]]; then
    echo "Error: path name missing"
    exit 1
fi

PACKAGE_NAME="$1"
PACKAGE_FOLDER="${PACKAGE_ROOT}/${PACKAGE_NAME}"


if [[ ! -d "${PACKAGE_FOLDER}" ]]; then
    echo "Error: folder '${PACKAGE_FOLDER}' not found"
    exit 2
fi

INSTALLER_NAME="OVVMaxx-Package-${PACKAGE_NAME}"
INSTALLER_NAME_EXTENSION=".sfx.sh"


echo "${text_bold}Writing tmp file with theme name${text_normal}"
echo "  creating temp path"
TMP_FOLDER=$(mktemp -d /tmp/.ovv.XXXXXXXX)
echo "    $TMP_FOLDER"
if [ "$?" != "0" ]; then
    echo "      Error: could not create temp folder"
    exit 13
fi
echo "      ok"

mkdir -p "${TMP_FOLDER}/_files"
mkdir -p "${TMP_FOLDER}/_scripts"

echo "  copying files to tmp folder"
cp -af "${PACKAGE_FOLDER}"/* "${TMP_FOLDER}/_files"
if [ "$?" != "0" ]; then
    echo "    Error"
    exit 13
fi
echo "    ok"

if [[ -f "${TMP_FOLDER}/_files/_INSTALLER_SCRIPT.post.sh" ]]; then
    echo "    found post install script, we don't want it in here"
    rm -rf "${TMP_FOLDER}/_files/_INSTALLER_SCRIPT.post.sh"
fi

echo "  copying scripts to tmp folder"
cp -af "${INSTALLER_SCRIPTS_PATH}"/* "${TMP_FOLDER}/_scripts"
if [ "$?" != "0" ]; then
    echo "    Error"
    exit 13
fi
echo "    ok"

echo "  copying post processing script to tmp folder"
echo "    looking for post install script"
echo "      ${PACKAGE_FOLDER}/_INSTALLER_SCRIPT.post.sh"
if [[ -f "${PACKAGE_FOLDER}/_INSTALLER_SCRIPT.post.sh" ]]; then
    cp -af "${PACKAGE_FOLDER}/_INSTALLER_SCRIPT.post.sh" "${TMP_FOLDER}/_scripts"
    if [ "$?" != "0" ]; then
        echo "    Error"
        exit 13
    fi
    echo "        added"
else
    echo "  not found, skipping"
fi


echo "${text_bold}Creating self-extracting archive script${text_normal}"
/usr/local/bin/makeself/makeself.sh "${TMP_FOLDER}" "${INSTALLER_NAME}${INSTALLER_NAME_EXTENSION}" "${INSTALLER_NAME}" ./_scripts/_INSTALLER_SCRIPT.init.sh


echo "${text_bold}Cleaning up${text_normal}"
rm -rf "${TMP_FOLDER}"


echo "${text_bold}Done${text_normal}"

