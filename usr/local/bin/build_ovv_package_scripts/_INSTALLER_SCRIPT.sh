#!/bin/bash

if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."

	exit 1
fi


INSTALLER_DATE=$(date "+%Y%m%d_%H%M%S")


echo "Starting installer"


SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "  script path"
echo "    ${SCRIPT_PATH}"


echo "Copying files"
FOLDER="${SCRIPT_PATH}/../_files"
while IFS= read -r -d '' -u 9
do
    FILE=$REPLY
    EXT=${FILE##*.}
    DEST=${FILE#"$FOLDER"}
    DEST_DIR=$(dirname ${DEST})

    echo "  source"
    echo "    ${FILE}"
    echo "  destination"
    echo "    ${DEST}"

    if [[ ! -d "${DEST_DIR}" ]]; then
        echo "  destination dir does not exist"
        echo "    creating"
        \mkdir -p -m 755 "${DEST_DIR}"
    fi

    if [[ -f "${DEST}" ]]; then
        echo "  already exists, backup needed"
        \cp -af "${DEST}" "${DEST}.backup.${INSTALLER_DATE}"
    fi

    echo "  copying"
    \cp -af "${FILE}" "${DEST}"

    \chown root:root "${DEST}"
    \chmod 644 "${DEST}"

    if [[ "${EXT}" == "php" ]]; then
        echo "  php file, setting execute permission"
        \chmod 755 "${DEST}"
    fi

    if [[ "${EXT}" == "sh" ]]; then
        echo "  shell script, setting execute permission"
        \chmod 755 "${DEST}"
    fi
done 9< <( find "${FOLDER}" -type f -exec printf '%s\0' {} + )


echo "Any post-processing needed?"
FOLDER="${SCRIPT_PATH}/../_scripts"
if [[ -f "${FOLDER}/_INSTALLER_SCRIPT.post.sh" ]]; then
    echo "  running ${FOLDER}/_INSTALLER_SCRIPT.post.sh"
    
    source "${FOLDER}/_INSTALLER_SCRIPT.post.sh"

    sleep 1

    rm -rf "${FOLDER}/_INSTALLER_SCRIPT.post.sh"
else
    echo "  no"
fi


echo "Done"
echo "  bye"
