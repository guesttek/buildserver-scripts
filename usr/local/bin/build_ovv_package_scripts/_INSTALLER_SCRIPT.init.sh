#!/bin/bash

if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."

	exit 1
fi

SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


INSTALLER_LOG_DATE=$(date "+%Y%m%d_%H%M%S")
INSTALLER_LOG_PATH="/mnt/kd/user/ovv/logs/OVV-Package"
INSTALLER_LOG_FILE="$INSTALLER_LOG_DATE.log"


echo "Initialising installer"
echo "  logfile"
echo "    $INSTALLER_LOG_PATH/$INSTALLER_LOG_FILE"
mkdir -p "$INSTALLER_LOG_PATH"

echo "  script path"
echo "    ${SCRIPT_PATH}"

source "${SCRIPT_PATH}"/_INSTALLER_SCRIPT.sh 2>&1 | tee "$INSTALLER_LOG_PATH/$INSTALLER_LOG_FILE"

